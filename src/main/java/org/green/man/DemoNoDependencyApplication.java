package org.green.man;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoNoDependencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoNoDependencyApplication.class, args);
	}
}
